package com.mock.dto

import kotlinx.serialization.Serializable
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

@Serializable
data class Message(
    val text: String,
    val time: String = LocalDateTime.now().format(DateTimeFormatter.ISO_DATE_TIME)
)
