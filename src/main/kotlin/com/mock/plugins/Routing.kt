package com.mock.plugins

import io.ktor.server.application.*
import io.ktor.server.request.*
import io.ktor.server.response.*
import io.ktor.server.routing.*

fun Application.configureRouting() {
    routing {
        get("/ping") {
            call.respondText(" {\"text\" : \"pong\", \"mext\" : \"text\"} ")
        }
    }

    routing {
        get("/bing") {
            call.respondText("bong")
        }
    }

    routing {
        post("/echo") {
            call.respondText(call.receiveText())
        }
    }

    routing {
        get("/query") {
            if (call.request.queryParameters["valid"] == "true") call.respondText(" {\"text\" : \"parameters are valid\"}")
            else call.respondText(" {\"text\" : \"parameters are invalid\"}")
        }
    }
}
