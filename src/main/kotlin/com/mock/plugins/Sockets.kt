package com.mock.plugins

import com.mock.dto.Connection
import com.mock.dto.Message
import io.ktor.serialization.*
import io.ktor.serialization.kotlinx.*
import io.ktor.websocket.*
import io.ktor.server.application.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import io.ktor.server.websocket.*
import kotlinx.serialization.json.Json
import java.time.*
import java.util.*
import kotlin.collections.LinkedHashSet

fun Application.configureSockets() {
    install(WebSockets) {
        pingPeriod = Duration.ofSeconds(15)
        timeout = Duration.ofSeconds(15)
        contentConverter = KotlinxWebsocketSerializationConverter(Json)
        maxFrameSize = Long.MAX_VALUE
        masking = false
    }

    routing {
        val connections = Collections.synchronizedSet<Connection?>(LinkedHashSet())
        webSocket("/chat") {
//            sendSerialized(Message("You need to register first"))
            val thisConnection = Connection(this)
            connections += thisConnection
            try {
//                send("You are connected! There are ${connections.count()} users here.")
                for (frame in incoming) {
                    if (frame.frameType == FrameType.TEXT || frame.frameType == FrameType.BINARY) {
                        val receivedMessage: Message? = this.converter?.deserialize(frame)
                        println(receivedMessage?.time)
                        connections.forEach {
                            it.session.sendSerialized(receivedMessage)
                        }
                    }
                }
            } catch (e: Exception) {
                println(e.localizedMessage)
            } finally {
                println("Removing $thisConnection!")
                connections -= thisConnection
            }
        }
    }
}
